import Vue from 'vue'
import './plugins/axios'
import App from './App.vue'
import router from './router'
import store from './store'
import './plugins/element.js'
import VueParticles from 'vue-particles'

Vue.use(VueParticles)
Vue.config.productionTip = false
Vue.config.silent = true
//使用钩子函数对路由进行权限跳转
// const whiteList = ['/login', '/auth-redirect']
router.beforeEach(async (to, from, next) => {
  const type = localStorage.getItem('type');
  // alert(type);
  if (type == 1 || type == 2) {
    console.log(11111);
    const accessRoutes = await store.dispatch('permission/generateRoutes', ["user"]);
    // router.addRoutes(accessRoutes)
    router.options.routes = accessRoutes
    router.addRoutes(accessRoutes);
    // next();
    next({ ...to, replace: true });
    localStorage.setItem('type',"12323233");
  }else {
    // if (whiteList.indexOf(to.path) !== -1) {
    //   // in the free login whitelist, go directly
    //   next();
    // } else {
    //   // other pages that do not have permission to access are redirected to the login page.
    //   next(`/login?redirect=${to.path}`);
    // }
    next();
  }
});


new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
