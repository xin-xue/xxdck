import Vue from 'vue'
import VueRouter from 'vue-router'


import Login from "../views/Login";
import Family from "../views/Family.vue"

import AddChiang from "../views/AddChiang.vue";
import AddEvent from "../views/AddEvent.vue";
import AddRRelation from "../views/AddRRelation.vue";

import DeleteChiang from "../views/DeleteChiang.vue";
import DeleteEvent from "../views/DeleteEvent.vue";
import DeleteRRelation from "../views/DeleteRRelation.vue";
import DeleteFamily from "../views/DeleteFamily.vue";

import FindChiang from "../views/FindChiang.vue";
import FindRRelation from "../views/FindRRelation.vue";
import FindEvent from "../views/FindEvent.vue";
import FindFamily from "../views/FindFamily.vue";
import FindAAA from "../views/FindAAA.vue";

import UpdateChiang from "../views/UpdateChiang.vue";
import UpdateEvent from "../views/UpdateEvent.vue";
import UpdateRRelation from "../views/UpdateRRelation.vue";
import UpdateFamily from "../views/UpdateFamily.vue";


import ShowFamilyTree from "../views/ShowFamilyTree.vue";
import ShowTree from "../views/ShowTree.vue";




Vue.use(VueRouter)

export const startLogin = [
  {
    path : '/',
    name : '登录',
    component : Login,
  }
]

export const familyLogin = [
  {
    path : '/Add',
    name : '添加',
    component : Family,
    children : [
      {
        path : '/AddRRelation',
        name : '添加成员关系',
        component : AddRRelation,
      },
      {
        path : '/AddChiang',
        name : '添加成员信息',
        component : AddChiang,
      },
      {
        path : '/AddEvent',
        name : '添加事件信息',
        component : AddEvent,
      }
    ]
  },
  {
    path: '/delete',
    name : '删除',
    component: Family,
    children: [
      {
        path : '/DeleteRRelation',
        name : '删除成员关系',
        component: DeleteRRelation,
      },
      {
        path: '/DeleteChiang',
        name : '删除成员信息',
        component: DeleteChiang,
      },
      {
        path: '/DeleteEvent',
        name : '删除成员事件',
        component: DeleteEvent,
      },
      {
        path : '/DeleteFamily',
        name : '删除族谱',
        component : DeleteFamily,
      }
    ]
  },
  {
    path: '/find',
    name : '查询',
    component: Family,
    children: [
      {
        path: '/FindFimaly',
        name:'查询家谱信息',
        component: FindFamily,
      },
      {
        path:'/FindRRelation',
        name:'查询成员关系',
        component: FindRRelation,
      },
      {
        path:'/FindChiang',
        name:'查询成员信息',
        component: FindChiang,
      },
      {
        path:'/FindEvent',
        name:'查询事件信息',
        component:FindEvent,
      },
      {
        path:'/FindAAA',
        name:'查询通用表',
        component:FindAAA,
      }
    ]
  },
  {
    path: '/update',
    name:'更新',
    component: Family,
    children: [
      {
        path: '/UpdateRRelation',
        name:'更新成员关系',
        component: UpdateRRelation,
      },
      {
        path: '/UpdateChiang',
        name:'更新成员信息',
        component: UpdateChiang,
      },
      {
        path: '/UpdateEvent',
        name:'更新事件信息',
        component: UpdateEvent,
      },
      {
        path : '/UpdateFamily',
        name : '更新族谱',
        component : UpdateFamily,
      }
    ]
  },
  {
    path: '/show',
    name:'展示',
    component: Family,
    children: [
      
      {
        path: '/ShowFamilyTree',
        name:'展示家族树形关系',
        component: ShowFamilyTree,
      },
      {
        path: '/ShowTree',
        name:'展示节点关系',
        component: ShowTree,
      }
    ]
  }
]



const router = new VueRouter({
  mode: 'hash',
  routes :startLogin
})

export default router

