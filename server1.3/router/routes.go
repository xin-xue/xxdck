package router

import (
	"m/Controller"
	"m/GoFile"
	"m/middleware"

	"github.com/gin-gonic/gin"
)

func CreateRoute(re *gin.Engine) *gin.Engine {
	re.Use(middleware.CORSMiddleware())
	GoFile.InsertFamily(re)    // 监视插入家族管理员
	GoFile.InsertRRelation(re) // 监视插入关系
	GoFile.InsertChiang(re)    // 监视插入成员信息
	GoFile.InsertEvent(re)     //监视成员事件
	GoFile.InsertAAA(re)       //监视通用表
	GoFile.RouterInit(re)      // 前端交互
	re.GET("/info", middleware.AuthMiddleware(), Controller.Info)
	return re
}
