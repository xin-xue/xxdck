package util

import (
	"math/rand"
	"time"
)

func RandomString(n int) string {
	var letter = []byte("1223344241")
	result := make([]byte, n)
	rand.Seed(time.Now().Unix())
	for i:=range letter {
		result[i] = letter[rand.Intn(len(letter))]
	}
	return string(result)
}
