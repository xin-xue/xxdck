package main

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"m/Global"
	"m/GoFile"
	"m/router"

)



func main() {
	v := GoFile.InitConfig()
	fmt.Println("配置文件读取结果：", v.Get("server.port"))

	var erre error
	Global.CDb, erre = GoFile.InitRedisDB(v)
	if erre != nil {
		fmt.Println("initredisdb执行失败", erre.Error())
		//return
	}

	var err error
	Global.Db, err = GoFile.InitDB(v)
	re := gin.Default()
	//re = AllRouter(re)
	if err != nil {
		fmt.Println("数据库连接失败！")
		return
	}
	re = router.CreateRoute(re)
	panic(re.Run(":" + v.GetString("server.port")))
}


