package Controller

import (
	"encoding/json"
	"fmt"
	"log"
	"m/Global"
	"m/model"
	"net/http"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

func RegisterAAA(context *gin.Context) {
	// 使用结构体来获取参数
	var requsetAAA = model.AAA{}
	json.NewDecoder(context.Request.Body).Decode(&requsetAAA)
	fmt.Println("获取到的数据：",
		requsetAAA.Ano,
		requsetAAA.Aname,
		requsetAAA.Bno,
		requsetAAA.Bname,
	)

	// 获取参数
	fmt.Println("开始获取通用表关系参数...")
	ano := requsetAAA.Ano
	aname := requsetAAA.Aname
	bno := requsetAAA.Bno
	bname := requsetAAA.Bname

	// 数据验证
	fmt.Println("开始验证关系数据...")
	fmt.Println("组ID:", ano, "组名称：", aname, "分项ID:", bno, "分项名称", bname)
	if len(ano) == 0 {

		fmt.Println("发现组ID为空")
		context.JSON(200, gin.H{
			"code": 200,
			"msg":  "组ID不能为空",
		})
		return
	}
	if len(aname) == 0 {

		fmt.Println("发现组名称为空")
		context.JSON(200, gin.H{
			"code": 200,
			"msg":  "组名称不能为空",
		})
		return
	}
	if len(bno) == 0 {

		fmt.Println("发现分项ID为空")
		context.JSON(200, gin.H{
			"code": 200,
			"msg":  "分项ID不能为空",
		})
		return
	}

	if len(bname) == 0 {

		fmt.Println("发现分项名称为空")
		context.JSON(200, gin.H{
			"code": 200,
			"msg":  "分项名称不能为空",
		})
		return
	}

	fmt.Println(ano, aname, bno, bname)
	log.Println(ano, aname, bno, bname)

	if isAAAExists(Global.Db, ano, bno) {

		context.JSON(200, gin.H{
			"code": 200,
			"msg":  "通用表已经存在",
		})
		return
	}

	newAAA := model.AAA{
		Ano:   ano,
		Aname: aname,
		Bno:   bno,
		Bname: bname,
	}
	Global.Db.Create(&newAAA)
	// 返回结果
	context.JSON(200, gin.H{
		"msg": "插入通用表成功",
	})
}
func FindAAA(context *gin.Context) {
	fmt.Println("进入findaaa")
	var aaa []model.AAA
	err := Global.Db.Find(&aaa).Error
	if err != nil {
		fmt.Println("查询通用表出错！")
		return
	}
	fmt.Println("数据库查到的通用表信息：", aaa)

	context.JSON(http.StatusOK, aaa)
}

func CountAAA(context *gin.Context) {
	// 调用存储过程
	var count int
	Global.Db.Raw("select count_aaa()").First(&count)
	fmt.Println("通用表事件类型数量：", count)
	context.JSON(200, gin.H{
		"code": 200,
		"msg":  "返回通用表数量成功",
		"data": count,
	})
}

func isAAAExists(db *gorm.DB, ano string, bno string) bool {
	var event model.Event
	err := db.Where("ano = ?", ano, "bno = ?", bno).Find(&event).Error
	if err != nil {
		fmt.Println("err执行")
		return false
	} else {
		fmt.Println("通用表执行了else")
		return false
	}
}
