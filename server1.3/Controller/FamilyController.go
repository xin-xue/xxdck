package Controller

import (
	"encoding/json"
	"fmt"
	"log"
	"m/Global"
	"m/model"
	"m/response"
	"net/http"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

func RegisterFamily(context *gin.Context) {
	// 获取参数
	fmt.Println("开始获取参数")
	var requestFamily = model.Family{}
	json.NewDecoder(context.Request.Body).Decode(&requestFamily)
	sno := requestFamily.Sno
	sname := requestFamily.Sname
	sdate := requestFamily.Sdate
	spassword := requestFamily.Spassword

	// 数据验证
	fmt.Println("开始数据验证")
	fmt.Println("传来的注册信息：", sno, sname, sdate, spassword)
	if len(sno) < 1 {

		context.JSON(200, gin.H{
			"code": 200,
			"msg":  "账号不能为空！",
		})
		return
	}
	if len(sname) == 0 {

		return
	}
	if len(spassword) < 6 {

		context.JSON(200, gin.H{
			"code": 200,
			"msg":  "密码不能少于6位！",
		})
		return
	}
	fmt.Println(sno, sname, sdate, spassword)
	log.Println(sno, sname, spassword, sdate)
	log.Println(sno)

	if isSnoExists(Global.Db, sno) {

		context.JSON(200, gin.H{
			"code": 200,
			"msg":  "此账号已被注册！",
		})
		// 直接返回，不执行下面的create了
		return
	}

	newFamily := model.Family{
		Sno:       sno,
		Sname:     sname,
		Sdate:     sdate,
		Spassword: spassword,
	}
	Global.Db.Create(&newFamily)

	// 发放token
	token, err := Global.ReaeaseTokec(newFamily)
	if err != nil {
		context.JSON(200, gin.H{
			"code": 200,
			"msg":  "系统异常",
		})
		log.Printf("token generate error:%v\n", err)
		return
	}

	response.Success(context, gin.H{"token": token}, "注册成功！")
}

func FamilyLogin(context *gin.Context) {
	fmt.Println("进入familylogin")

	var requestFamily = model.Family{}
	json.NewDecoder(context.Request.Body).Decode(&requestFamily) //法一
	//context.Bind(&requestFamily)//法二
	fmt.Println("获取到的数据：", requestFamily.Sno, requestFamily.Spassword)
	Global.Sno = requestFamily.Sno
	// 获取参数
	sno := requestFamily.Sno
	spassword := requestFamily.Spassword

	if len(sno) < 1 {
		context.JSON(200, gin.H{
			"code": 200,
			"msg":  "账号不能为空",
		})
		return
	}

	if len(spassword) < 6 {
		fmt.Println("密码不能少于6位！", len(spassword), len(requestFamily.Spassword))
		context.JSON(200, gin.H{
			"code": 200,
			"msg":  "密码不能少于6位",
		})
		return
	}
	// 学号是否存在
	var family model.Family

	Global.Db.Where("sno = ?", sno).First(&family)
	fmt.Println("family.Sno=", family.Sno)
	if family.Sno == "" {

		context.JSON(200, gin.H{
			"code": 200,
			"msg":  "用户不存在",
		})
		return
	}

	if family.Spassword != spassword {
		fmt.Println(family.Spassword, spassword)
		context.JSON(200, gin.H{
			"code": 200,
			"msg":  "密码错误",
		})
		return
	}
	// 发放token
	token, err := Global.ReaeaseTokec(family)
	if err != nil {
		context.JSON(200, gin.H{
			"code": 200,
			"msg":  "token发放失败?",
		})
		log.Printf("token generate error:%v\n", err)
		return
	}

	response.Success(context, gin.H{"token": token}, "登陆成功")
}

func Info(context *gin.Context) {
	family, _ := context.Get("user")
	context.JSON(http.StatusOK, gin.H{
		"code": 200,
		"data": gin.H{
			"user": family,
		},
	})
}

func FindFamily(context *gin.Context) {
	fmt.Println("进入findfamily")
	var family []model.Family
	err := Global.Db.Find(&family).Error
	if err != nil {
		fmt.Println("查询全体事件出错！")
		return
	}
	fmt.Println("数据库查到的全体事件信息：", family)
	context.JSON(http.StatusOK, family)
}

func CountFamily(context *gin.Context) {
	// 调用存储过程
	var count int
	Global.Db.Raw("select count_family()").First(&count)
	fmt.Println("家族数量：", count)
	context.JSON(200, gin.H{
		"code": 200,
		"msg":  "返回家族数量成功",
		"data": count,
	})
}

func FindFamilySelect(context *gin.Context) {
	fmt.Println("进入findfamilyselect")
	var requestFamily = model.Family{}
	json.NewDecoder(context.Request.Body).Decode(&requestFamily)
	fmt.Println("即将搜索账号：", requestFamily.Sno)

	if len(requestFamily.Sno) < 1 {
		fmt.Println("账号不能为空！", len(requestFamily.Sno))
		context.JSON(http.StatusUnprocessableEntity, gin.H{
			"code": 422,
			"msg":  "账号为空",
		})
		return
	}
	// 学号是否存在
	var family []model.Family
	Global.Db.Where("sno = ?", requestFamily.Sno).First(&family)
	fmt.Println("family.Sno=", family[0].Sno)
	if family[0].Sno == "" {
		context.JSON(http.StatusUnprocessableEntity, gin.H{
			"code": 422,
			"msg":  "找不到此家族！",
		})
		return
	}
	context.JSON(http.StatusOK, family)
}

func EditFamily(context *gin.Context) {
	fmt.Println("进入editfamily")
	var requestFamily = model.Family{}
	json.NewDecoder(context.Request.Body).Decode(&requestFamily)
	fmt.Println("即将编辑家族名：", requestFamily)

	if len(requestFamily.Sno) < 1 {
		fmt.Println("家族编号不能为空！", len(requestFamily.Sno))
		context.JSON(200, gin.H{
			"code": 200,
			"msg":  "家族编号不能为空",
		})
		return
	}
	if len(requestFamily.Sname) < 1 {
		fmt.Println("家族姓名不能为空！", len(requestFamily.Sname))
		context.JSON(200, gin.H{
			"code": 200,
			"msg":  "家族姓名不能为空",
		})
		return
	}
	if len(requestFamily.Sdate) < 1 {
		fmt.Println("注册日期不能为空！", len(requestFamily.Sdate))
		context.JSON(200, gin.H{
			"code": 200,
			"msg":  "注册日期不能为空",
		})
		return
	}
	if len(requestFamily.Spassword) < 1 {
		fmt.Println("死亡日期不能为空！", len(requestFamily.Spassword))
		context.JSON(200, gin.H{
			"code": 200,
			"msg":  "死亡日期不能为空",
		})
		return
	}

	// 课程号是否存在
	var family model.Family
	Global.Db.Where("sno = ?", requestFamily.Sno).First(&family)
	fmt.Println("family.Sno=", family.Sno)
	if family.Sname == "" {
		context.JSON(200, gin.H{
			"code": 200,
			"msg":  "找不到此家族",
		})
		return
	}

	err := Global.Db.Where("sno = ?", requestFamily.Sno).Updates(&requestFamily).Error
	if err != nil {
		fmt.Println("更新家族信息出错")
		context.JSON(200, gin.H{
			"code": 200,
			"msg":  "更新家族信息出错",
		})
		return
	}

	context.JSON(200, gin.H{
		"code": 200,
		"msg":  "更新成功",
	})
}

func DeleteFamily(context *gin.Context) {
	fmt.Println("进入deletefamily")
	var requestFamily = model.Family{}
	json.NewDecoder(context.Request.Body).Decode(&requestFamily)
	fmt.Println("即将删除家族编号：", requestFamily.Sno)

	if len(requestFamily.Sno) < 1 {
		fmt.Println("家族编号不能为空！", len(requestFamily.Sno))
		context.JSON(200, gin.H{
			"code": 200,
			"msg":  "家族编号不能为空",
		})
		return
	}
	// 课程号是否存在
	var family model.Family
	Global.Db.Where("sno = ?", requestFamily.Sno).Find(&family)
	fmt.Println("family.Sno=", family.Sno)
	if family.Sname == "" {
		context.JSON(200, gin.H{
			"code": 200,
			"msg":  "找不到此家族",
		})
		return
	}
	err := Global.Db.Where("sno = ?", requestFamily.Sno).Delete(&family).Error
	if err != nil {
		context.JSON(200, gin.H{
			"code": 200,
			"msg":  "删除家族信息出错",
		})
		return
	}

	context.JSON(200, gin.H{
		"code": 200,
		"msg":  "删除成功",
	})
}

func isSnoExists(db *gorm.DB, sno string) bool {
	var family model.Family

	err := db.Where("sno = ?", sno).First(&family).Error
	if err != nil {
		fmt.Println("err执行")
		return false
	}
	if family.Sno != "" {
		fmt.Println("找到的重复学号为：", family.Sno)
		fmt.Println("执行了if")
		return true
	} else {
		fmt.Println("执行了else")
		return false
	}
}
