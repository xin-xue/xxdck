package Controller

import (
	"encoding/json"
	"fmt"
	"log"
	"m/Global"
	"m/model"
	"net/http"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

func RegisterCourss(context *gin.Context) {
	// 使用结构体来获取参数
	var requestChiang = model.Chiang{}
	json.NewDecoder(context.Request.Body).Decode(&requestChiang)
	fmt.Println("获取到的数据：",
		requestChiang.Cno,
		requestChiang.Sno,
		requestChiang.Cname,
		requestChiang.Cbirth,
		requestChiang.Cdeath,
		requestChiang.Cbiography,
		requestChiang.Chonor,
	)
	// 获取参数
	fmt.Println("开始获取成员信息参数...")
	cno := requestChiang.Cno
	sno := requestChiang.Sno
	cname := requestChiang.Cname
	cbirth := requestChiang.Cbirth
	cdeath := requestChiang.Cdeath
	cbiography := requestChiang.Cbiography
	chonor := requestChiang.Chonor
	// 数据验证
	fmt.Println("开始验证成员信息数据...")

	if len(cno) == 0 {

		fmt.Println("发现成员编号为空")
		context.JSON(200, gin.H{
			"code": 200,
			"msg":  "成员编号不能为空",
		})
		return
	}

	fmt.Println(cno, sno, cname, cbirth, cdeath, cbiography, chonor)
	log.Println(cno, sno, cname, cbirth, cdeath, cbiography, chonor)
	// 判断课程号是否存在
	var err error

	if isCnoExists(Global.Db, cno) {

		context.JSON(200, gin.H{
			"code": 200,
			"msg":  "成员已存在",
		})
		// 直接返回，不执行下面的create了
		return
	}

	// 创建课程
	if err != nil {
		fmt.Println("错误")
		return
	}
	newChiang := model.Chiang{
		Cno:        cno,
		Sno:        sno,
		Cname:      cname,
		Cbirth:     cbirth,
		Cdeath:     cdeath,
		Cbiography: cbiography,
		Chonor:     chonor,
	}
	fmt.Println("newChiang:", newChiang.Cno, newChiang.Sno, newChiang.Cname, newChiang.Cbirth, newChiang.Cdeath, newChiang.Cbiography, newChiang.Chonor)
	Global.Db.Create(&newChiang)
	// 返回结果
	context.JSON(200, gin.H{
		"msg": "插入成员信息成功",
	})
}

func FindChiang(context *gin.Context) {
	fmt.Println("进入findchiang")
	var chiang []model.Chiang
	err := Global.Db.Find(&chiang).Error
	if err != nil {
		fmt.Println("查询全体成员出错！")
		return
	}
	fmt.Println("数据库查到的全体课程信息：", chiang)
	context.JSON(http.StatusOK, chiang)
}

func CountChiang(context *gin.Context) {
	// 调用存储过程
	var count int
	Global.Db.Raw("select count_chiang()").First(&count)
	fmt.Println("成员数量：", count)
	context.JSON(200, gin.H{
		"code": 200,
		"msg":  "返回成员数量成功",
		"data": count,
	})
	//context.JSON(http.StatusOK, count)
}

func EditChiang(context *gin.Context) {
	fmt.Println("进入editchiang")
	var requestChiang = model.Chiang{}
	json.NewDecoder(context.Request.Body).Decode(&requestChiang)
	fmt.Println("即将编辑成员名：", requestChiang)

	if len(requestChiang.Cno) < 1 {
		fmt.Println("成员编号不能为空！", len(requestChiang.Cno))
		context.JSON(200, gin.H{
			"code": 200,
			"msg":  "成员编号不能为空",
		})
		return
	}

	if len(requestChiang.Sno) < 1 {
		fmt.Println("成员编号不能为空！", len(requestChiang.Sno))
		context.JSON(200, gin.H{
			"code": 200,
			"msg":  "成员编号不能为空",
		})
		return
	}
	if len(requestChiang.Cname) < 1 {
		fmt.Println("成员姓名不能为空！", len(requestChiang.Cname))
		context.JSON(200, gin.H{
			"code": 200,
			"msg":  "成员姓名不能为空",
		})
		return
	}
	if len(requestChiang.Cbirth) < 1 {
		fmt.Println("出生日期不能为空！", len(requestChiang.Cbirth))
		context.JSON(200, gin.H{
			"code": 200,
			"msg":  "出生日期不能为空",
		})
		return
	}
	if len(requestChiang.Cdeath) < 1 {
		fmt.Println("死亡日期不能为空！", len(requestChiang.Cdeath))
		context.JSON(200, gin.H{
			"code": 200,
			"msg":  "死亡日期不能为空",
		})
		return
	}
	if len(requestChiang.Cbiography) < 1 {
		fmt.Println("生平简介不能为空！", len(requestChiang.Cbiography))
		context.JSON(200, gin.H{
			"code": 200,
			"msg":  "生平简介不能为空",
		})
		return
	}
	if len(requestChiang.Chonor) < 1 {
		fmt.Println("荣誉不能为空！", len(requestChiang.Chonor))
		context.JSON(200, gin.H{
			"code": 200,
			"msg":  "荣誉不能为空",
		})
		return
	}

	// 课程号是否存在
	var chiang model.Chiang
	Global.Db.Where("cno = ?", requestChiang.Cno).First(&chiang)
	fmt.Println("chiang.Cno=", chiang.Cno)
	if chiang.Cname == "" {
		context.JSON(200, gin.H{
			"code": 200,
			"msg":  "找不到此成员",
		})
		return
	}

	err := Global.Db.Where("cno = ?", requestChiang.Cno).Updates(&requestChiang).Error
	if err != nil {
		fmt.Println("更新成员信息出错")
		context.JSON(200, gin.H{
			"code": 200,
			"msg":  "更新成员信息出错",
		})
		return
	}

	context.JSON(200, gin.H{
		"code": 200,
		"msg":  "更新成功",
	})
}

func DeleteChiang(context *gin.Context) {
	fmt.Println("进入deletechiang")
	var requestChiang = model.Chiang{}
	json.NewDecoder(context.Request.Body).Decode(&requestChiang)
	fmt.Println("即将删除成员编号：", requestChiang.Cno)

	if len(requestChiang.Cno) < 1 {
		fmt.Println("成员编号不能为空！", len(requestChiang.Cno))
		context.JSON(200, gin.H{
			"code": 200,
			"msg":  "成员编号不能为空",
		})
		return
	}
	// 课程号是否存在
	var chiang model.Chiang
	Global.Db.Where("cno = ?", requestChiang.Cno).Find(&chiang)
	fmt.Println("chiang.Cno=", chiang.Cno)
	if chiang.Cname == "" {
		context.JSON(200, gin.H{
			"code": 200,
			"msg":  "找不到此成员",
		})
		return
	}
	// 首先删除这个成员所经历的事件
	var course = model.Event{
		Cno: requestChiang.Cno,
	}
	var err error
	err = Global.Db.Where("cno = ?", requestChiang.Cno).Delete(&course).Error
	if err != nil {
		context.JSON(200, gin.H{
			"code": 200,
			"msg":  "删除此成员事件出错",
		})
		return
	}

	//其次删除成员信息
	err = Global.Db.Where("cno = ?", requestChiang.Cno).Delete(&chiang).Error
	if err != nil {
		context.JSON(200, gin.H{
			"code": 200,
			"msg":  "删除成员信息出错",
		})
		return
	}

	context.JSON(200, gin.H{
		"code": 200,
		"msg":  "删除成功",
	})
}

func FindChiangSelect(context *gin.Context) {
	fmt.Println("进入showteacheerselect")
	var requestChiang = model.Chiang{}
	json.NewDecoder(context.Request.Body).Decode(&requestChiang)
	fmt.Println("即将搜索成员号：", requestChiang.Cno)

	if len(requestChiang.Cno) < 1 {
		fmt.Println("成员号不能为空！", len(requestChiang.Cno))
		context.JSON(http.StatusUnprocessableEntity, gin.H{
			"code": 422,
			"msg":  "成员号为空",
		})
		return
	}
	// 成员号是否存在
	var chiang []model.Chiang
	Global.Db.Where("cno = ?", requestChiang.Cno).First(&chiang)
	fmt.Println("chiang.Cno=", chiang[0].Cno)
	if chiang[0].Cno == "" {
		context.JSON(http.StatusUnprocessableEntity, gin.H{
			"code": 422,
			"msg":  "找不到此成员！",
		})
		return
	}
	context.JSON(http.StatusOK, chiang)

}

func FindChiangDelete(context *gin.Context) {
	fmt.Println("进入showchiangdelete")
	//var requesetChiang = model.Chiang{}
}
func isCnoExists(db *gorm.DB, cno string) bool {
	var chiang model.Chiang
	err := db.Where("cno = ?", cno).Find(&chiang).Error
	if err != nil {
		fmt.Println("err执行")
		return false
	}
	if chiang.Cname != "" { // 找到重复的课程号
		fmt.Println("找到重复的成员编号为：", chiang.Cno)
		fmt.Println("成员执行了if")
		return true
	} else {
		fmt.Println("成员执行了else")
		return false
	}
}
