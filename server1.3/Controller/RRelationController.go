package Controller

import (
	"encoding/json"
	"fmt"
	"log"
	"m/Global"
	"m/model"
	"net/http"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

func RegisterRRelation(context *gin.Context) {
	// 使用结构体来获取参数
	var requsetRRelation = model.RRelation{}
	json.NewDecoder(context.Request.Body).Decode(&requsetRRelation)
	fmt.Println("获取到的数据：",
		requsetRRelation.Cno1,
		requsetRRelation.Cno2,
		requsetRRelation.Zno,
	)

	// 获取参数
	fmt.Println("开始获取成员关系参数...")
	cno1 := requsetRRelation.Cno1
	cno2 := requsetRRelation.Cno2
	zno := requsetRRelation.Zno
	// 数据验证
	fmt.Println("开始验证关系数据...")
	fmt.Println("成员编号：", cno1, "成员编号：", cno2, "成员关系：", zno)
	if len(cno1) == 0 {

		fmt.Println("发现成员编号1为空")
		context.JSON(200, gin.H{
			"code": 200,
			"msg":  "成员编号1不能为空",
		})
		return
	}
	if len(cno2) == 0 {

		fmt.Println("发现成员编号2为空")
		context.JSON(200, gin.H{
			"code": 200,
			"msg":  "成员编号2不能为空",
		})
		return
	}
	if len(zno) == 0 {

		fmt.Println("发现成员关系为空")
		context.JSON(200, gin.H{
			"code": 200,
			"msg":  "成员关系不能为空",
		})
		return
	}
	fmt.Println(cno1, cno2, zno)
	log.Println(cno1, cno2, zno)

	if isTnoExists(Global.Db, cno1, cno2, zno) {

		context.JSON(200, gin.H{
			"code": 200,
			"msg":  "成员关系已经存在",
		})
		return
	}

	newRRelation := model.RRelation{
		Cno1: cno1,
		Cno2: cno2,
		Zno:  zno,
	}
	Global.Db.Create(&newRRelation)
	// 返回结果
	context.JSON(200, gin.H{
		"msg": "插入关系成功",
	})
}
func FindRRelation(context *gin.Context) {
	fmt.Println("进入findrrelation")
	var rrelation []model.RRelation
	err := Global.Db.Find(&rrelation).Error
	if err != nil {
		fmt.Println("查询全体关系出错！")
		return
	}
	fmt.Println("数据库查到的全体关系信息：", rrelation)

	//token, err := Global.ReaeaseTokec3(rrelation)
	context.JSON(http.StatusOK, rrelation)
}

func CountRRelation(context *gin.Context) {
	// 调用存储过程
	var count int
	Global.Db.Raw("select count_rrelation()").First(&count)
	fmt.Println("关系数量：", count)
	context.JSON(200, gin.H{
		"code": 200,
		"msg":  "返回关系数量成功",
		"data": count,
	})
}

func DeleteRRelation(context *gin.Context) {
	fmt.Println("进入deleterrelation")
	var requestRRelation = model.RRelation{}
	json.NewDecoder(context.Request.Body).Decode(&requestRRelation)
	fmt.Println("即将删除成员编号：", requestRRelation.Cno1, "即将删除成员编号：", requestRRelation.Cno2, "即将删除成员关系", requestRRelation.Zno)

	if len(requestRRelation.Cno1) < 1 {
		fmt.Println("成员编号不能为空！", len(requestRRelation.Cno1))
		context.JSON(200, gin.H{
			"code": 200,
			"msg":  "成员编号不能为空",
		})
		return
	}
	if len(requestRRelation.Cno2) < 1 {
		fmt.Println("成员编号不能为空！", len(requestRRelation.Cno2))
		context.JSON(200, gin.H{
			"code": 200,
			"msg":  "成员编号不能为空",
		})
		return
	}
	if len(requestRRelation.Zno) < 1 {
		fmt.Println("成员关系不能为空！", len(requestRRelation.Zno))
		context.JSON(200, gin.H{
			"code": 200,
			"msg":  "成员关系不能为空",
		})
		return
	}

	var rrelation model.RRelation
	Global.Db.Where("cno1 = ?", requestRRelation.Cno1, "cno2 = ?", requestRRelation.Cno2, "zno = ?", requestRRelation.Zno).Find(&rrelation)
	fmt.Println("rrelation.Cno1=", rrelation.Cno1, "rrelation.Cno2=", rrelation.Cno2, "rrelation.zno=", rrelation.Zno)
	if rrelation.Cno1 == "" {
		context.JSON(200, gin.H{
			"code": 200,
			"msg":  "找不到此成员",
		})
		return
	}
	if rrelation.Cno2 == "" {
		context.JSON(200, gin.H{
			"code": 200,
			"msg":  "找不到此成员",
		})
		return
	}
	if rrelation.Zno == "" {
		context.JSON(200, gin.H{
			"code": 200,
			"msg":  "找不到此成员关系",
		})
		return
	}

	err := Global.Db.Where("cno1 = ?", requestRRelation.Cno1, "cno2 = ?", requestRRelation.Cno2, "zno = ?", requestRRelation.Zno).Delete(&rrelation).Error
	if err != nil {
		context.JSON(200, gin.H{
			"code": 200,
			"msg":  "删除成员信息出错",
		})
		return
	}

	context.JSON(200, gin.H{
		"code": 200,
		"msg":  "删除成功",
	})
}

func EditRRelation(context *gin.Context) {
	fmt.Println("进入UpdateRRelation")
	var requestRRelation = model.RRelation{}
	json.NewDecoder(context.Request.Body).Decode(&requestRRelation)
	fmt.Println("即将编辑成员编号1：", requestRRelation.Cno1, "即将编辑成员编号2：", requestRRelation.Cno2, "即将编辑成员关系", requestRRelation.Zno)

	if len(requestRRelation.Cno1) < 1 {
		fmt.Println("成员编号1不能为空！", len(requestRRelation.Cno1))
		context.JSON(200, gin.H{
			"code": 200,
			"msg":  "成员编号1不能为空",
		})
		return
	}
	if len(requestRRelation.Cno2) < 1 {
		fmt.Println("成员编号2不能为空！", len(requestRRelation.Cno2))
		context.JSON(200, gin.H{
			"code": 200,
			"msg":  "成员编号2不能为空",
		})
		return
	}
	if len(requestRRelation.Zno) < 1 {
		fmt.Println("成员关系不能为空！", len(requestRRelation.Zno))
		context.JSON(200, gin.H{
			"code": 200,
			"msg":  "成员关系不能为空",
		})
		return
	}

	var rrelation model.RRelation
	Global.Db.Where("cno1 = ?", requestRRelation.Cno1, "cno2 = ?", requestRRelation.Cno2).First(&rrelation)
	fmt.Println("rrelation.Cno1=", rrelation.Cno1, "rrelation.Cno2=", rrelation.Cno2)
	if rrelation.Cno1 == "" {
		context.JSON(200, gin.H{
			"code": 200,
			"msg":  "找不到此成员",
		})
		return
	}
	if rrelation.Cno2 == "" {
		context.JSON(200, gin.H{
			"code": 200,
			"msg":  "找不到此成员",
		})
		return
	}

	err := Global.Db.Where("cno1 = ?", requestRRelation.Cno1, "cno2 = ?", requestRRelation.Cno2).Updates(&requestRRelation).Error
	if err != nil {
		fmt.Println("更新成员关系出错")
		context.JSON(200, gin.H{
			"code": 200,
			"msg":  "更新成员关系出错",
		})
		return
	}
	context.JSON(200, gin.H{
		"code": 200,
		"msg":  "更新成功",
	})

}

func isTnoExists(db *gorm.DB, cno1 string, cno2 string, zno string) bool {
	var rrelation model.RRelation
	err := db.Where("cno1 = ?", cno1, "cno2 = ?", cno2, "zno = ?", zno).Find(&rrelation).Error
	if err != nil {
		fmt.Println("err执行")
		return false
	} else {
		fmt.Println("成员执行了else")
		return false
	}
}
