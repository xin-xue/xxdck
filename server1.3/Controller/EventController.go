package Controller

import (
	"encoding/json"
	"fmt"
	"log"
	"m/Global"
	"m/model"
	"net/http"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

func RegisterEvent(context *gin.Context) {
	// 使用结构体来获取参数
	var requsetEvent = model.Event{}
	json.NewDecoder(context.Request.Body).Decode(&requsetEvent)
	fmt.Println("获取到的数据：",
		requsetEvent.Cno,
		requsetEvent.Eno,
		requsetEvent.Edate,
		requsetEvent.Edescrible,
		requsetEvent.Eattach,
	)

	// 获取参数
	fmt.Println("开始获取成员关系参数...")
	cno := requsetEvent.Cno
	eno := requsetEvent.Eno
	edate := requsetEvent.Edate
	edescrible := requsetEvent.Edescrible
	eattach := requsetEvent.Eattach
	// 数据验证
	fmt.Println("开始验证关系数据...")
	fmt.Println("成员编号：", cno, "事件编号：", eno, "事件时间：", edate, "事件描述", edescrible, "事件附件", eattach)
	if len(cno) == 0 {

		fmt.Println("发现成员编号为空")
		context.JSON(200, gin.H{
			"code": 200,
			"msg":  "成员编号不能为空",
		})
		return
	}
	if len(eno) == 0 {

		fmt.Println("发现事件编号为空")
		context.JSON(200, gin.H{
			"code": 200,
			"msg":  "事件编号不能为空",
		})
		return
	}
	if len(edate) == 0 {

		fmt.Println("发现事件时间为空")
		context.JSON(200, gin.H{
			"code": 200,
			"msg":  "事件时间不能为空",
		})
		return
	}

	if len(edescrible) == 0 {

		fmt.Println("发现事件描述为空")
		context.JSON(200, gin.H{
			"code": 200,
			"msg":  "事件描述不能为空",
		})
		return
	}
	if len(eattach) == 0 {

		fmt.Println("发现事件附件为空")
		context.JSON(200, gin.H{
			"code": 200,
			"msg":  "事件附件不能为空",
		})
		return
	}
	fmt.Println(cno, eno, edate, edescrible, eattach)
	log.Println(cno, eno, edate, edescrible, eattach)

	if isEventExists(Global.Db, cno, eno) {

		context.JSON(200, gin.H{
			"code": 200,
			"msg":  "成员事件已经存在",
		})
		return
	}

	newEvent := model.Event{
		Cno:        cno,
		Eno:        eno,
		Edate:      edate,
		Edescrible: edescrible,
		Eattach:    eattach,
	}
	Global.Db.Create(&newEvent)
	// 返回结果
	context.JSON(200, gin.H{
		"msg": "插入成员事件成功",
	})
}
func FindEvent(context *gin.Context) {
	fmt.Println("进入findevent")
	var event []model.Event
	err := Global.Db.Find(&event).Error
	if err != nil {
		fmt.Println("查询全体事件出错！")
		return
	}
	fmt.Println("数据库查到的全体事件信息：", event)

	context.JSON(http.StatusOK, event)
}

func CountEvent(context *gin.Context) {
	// 调用存储过程
	var count int
	Global.Db.Raw("select count_event()").First(&count)
	fmt.Println("事件数量：", count)
	context.JSON(200, gin.H{
		"code": 200,
		"msg":  "返回事件数量成功",
		"data": count,
	})
}

func DeleteEvent(context *gin.Context) {
	fmt.Println("进入deleteevent")
	var requestEvent = model.Event{}
	json.NewDecoder(context.Request.Body).Decode(&requestEvent)
	fmt.Println("即将删除成员编号：", requestEvent.Cno, "即将删除事件编号：", requestEvent.Eno)

	if len(requestEvent.Cno) < 1 {
		fmt.Println("成员编号不能为空！", len(requestEvent.Cno))
		context.JSON(200, gin.H{
			"code": 200,
			"msg":  "成员编号不能为空",
		})
		return
	}
	if len(requestEvent.Eno) < 1 {
		fmt.Println("事件编号不能为空！", len(requestEvent.Eno))
		context.JSON(200, gin.H{
			"code": 200,
			"msg":  "事件编号不能为空",
		})
		return
	}

	var event model.Event
	Global.Db.Where("cno = ?", requestEvent.Cno, "eno = ?", requestEvent.Eno).Find(&event)
	fmt.Println("event.Cno=", event.Cno, "event.Eno=", event.Eno)
	if event.Cno == "" {
		context.JSON(200, gin.H{
			"code": 200,
			"msg":  "找不到此成员",
		})
		return
	}
	if event.Eno == "" {
		context.JSON(200, gin.H{
			"code": 200,
			"msg":  "找不到此事件",
		})
		return
	}

	err := Global.Db.Where("cno = ?", requestEvent.Cno, "eno = ?", requestEvent.Eno).Delete(&event).Error
	if err != nil {
		context.JSON(200, gin.H{
			"code": 200,
			"msg":  "删除事件信息出错",
		})
		return
	}

	context.JSON(200, gin.H{
		"code": 200,
		"msg":  "删除成功",
	})
}

func EditEvent(context *gin.Context) {
	fmt.Println("进入UpdateEvent")
	var requestEvent = model.Event{}
	json.NewDecoder(context.Request.Body).Decode(&requestEvent)
	fmt.Println("即将编辑成员编号：", requestEvent.Cno, "即将编辑事件编号：", requestEvent.Eno, "即将编辑事件日期：", requestEvent.Edate, "即将编辑事件描述：", requestEvent.Edescrible, "即将编辑事件附件：", requestEvent.Eattach)

	if len(requestEvent.Cno) < 1 {
		fmt.Println("成员编号不能为空！", len(requestEvent.Cno))
		context.JSON(200, gin.H{
			"code": 200,
			"msg":  "成员编号不能为空",
		})
		return
	}
	if len(requestEvent.Eno) < 1 {
		fmt.Println("事件编号不能为空！", len(requestEvent.Eno))
		context.JSON(200, gin.H{
			"code": 200,
			"msg":  "事件编号不能为空",
		})
		return
	}
	if len(requestEvent.Edate) < 1 {
		fmt.Println("事件日期不能为空！", len(requestEvent.Edate))
		context.JSON(200, gin.H{
			"code": 200,
			"msg":  "事件日期不能为空",
		})
		return
	}
	if len(requestEvent.Edescrible) < 1 {
		fmt.Println("事件描述不能为空！", len(requestEvent.Edescrible))
		context.JSON(200, gin.H{
			"code": 200,
			"msg":  "事件描述不能为空",
		})
		return
	}
	if len(requestEvent.Eattach) < 1 {
		fmt.Println("事件附件不能为空！", len(requestEvent.Eattach))
		context.JSON(200, gin.H{
			"code": 200,
			"msg":  "事件附件不能为空",
		})
		return
	}

	var event model.Event
	Global.Db.Where("cno = ?", requestEvent.Cno, "eno = ?", requestEvent.Eno).First(&event)
	fmt.Println("event.Cno=", event.Cno, "event.Eno=", event.Eno)
	if event.Cno == "" {
		context.JSON(200, gin.H{
			"code": 200,
			"msg":  "找不到此成员",
		})
		return
	}
	if event.Eno == "" {
		context.JSON(200, gin.H{
			"code": 200,
			"msg":  "找不到此事件",
		})
		return
	}

	err := Global.Db.Where("cno = ?", requestEvent.Cno, "eno = ?", requestEvent.Eno).Updates(&requestEvent).Error
	if err != nil {
		fmt.Println("更新成员事件出错")
		context.JSON(200, gin.H{
			"code": 200,
			"msg":  "更新成员事件出错",
		})
		return
	}
	context.JSON(200, gin.H{
		"code": 200,
		"msg":  "更新成功",
	})

}

func isEventExists(db *gorm.DB, cno string, eno string) bool {
	var event model.Event
	err := db.Where("cno = ?", cno, "eno = ?", eno).Find(&event).Error
	if err != nil {
		fmt.Println("err执行")
		return false
	} else {
		fmt.Println("成员执行了else")
		return false
	}
}
