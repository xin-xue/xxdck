package GoFile

import (
	"m/Controller"

	"github.com/gin-gonic/gin"
)

func InsertAAA(re *gin.Engine) {
	re.POST("/InsertAAA", Controller.RegisterAAA)
	re.POST("/FindAAA", Controller.FindAAA)
	re.POST("/CountAAA", Controller.CountAAA)
}
