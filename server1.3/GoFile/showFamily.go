package GoFile

import (
	"fmt"
	"m/Global"
	"m/model"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
)

func RouterInit(router *gin.Engine) {
	router.Use(cors.Default())

	router.GET("/find/FindFimaly", GetList)
}

func GetList(c *gin.Context) {
	var family model.Family
	err := Global.Db.Take(&family).Error
	if err != nil {
		fmt.Println("查询错误！！！")
		return
	}

	c.JSON(0, family)
}
