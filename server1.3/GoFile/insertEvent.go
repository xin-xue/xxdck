package GoFile

import (
	"m/Controller"

	"github.com/gin-gonic/gin"
)

func InsertEvent(re *gin.Engine) {
	re.POST("/InsertEvent", Controller.RegisterEvent)
	re.POST("/FindEvent", Controller.FindEvent)
	re.POST("/DeleteEvent", Controller.DeleteEvent)
	re.POST("/EditEvent", Controller.EditEvent)
	re.POST("/CountEvent", Controller.CountEvent)

}
