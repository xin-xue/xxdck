package GoFile

import (
	"m/Controller"

	"github.com/gin-gonic/gin"
)

func InsertChiang(re *gin.Engine) {
	re.POST("/InsertChiang", Controller.RegisterCourss)
	re.POST("/EditChiang", Controller.EditChiang)
	re.POST("/DeleteChiang", Controller.DeleteChiang)
	re.POST("/CountChiang", Controller.CountChiang)
	re.POST("/FindChiang", Controller.FindChiang)
	re.POST("/FindChiangSelect", Controller.FindChiangSelect)
	re.POST("/FindChiangDelete", Controller.FindChiangDelete)
}
