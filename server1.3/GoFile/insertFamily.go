package GoFile

import (
	"m/Controller"

	"github.com/gin-gonic/gin"
)

func InsertFamily(re *gin.Engine) {
	//re := gin.Default()
	re.POST("/InsertFamily", Controller.RegisterFamily)
	re.POST("/LoginFamily", Controller.FamilyLogin)
	re.POST("/FindFamilySelect", Controller.FindFamilySelect)
	re.POST("/FindFamily", Controller.FindFamily)
	re.POST("/CountFamily", Controller.CountFamily)
	re.POST("/DeleteFamily", Controller.DeleteFamily)
	re.POST("/EditFamily", Controller.EditFamily)

}
