package GoFile

import (
	"m/Controller"

	"github.com/gin-gonic/gin"
)

func InsertRRelation(re *gin.Engine) {
	re.POST("/InsertRRelation", Controller.RegisterRRelation)
	re.POST("/FindRRelation", Controller.FindRRelation)
	re.POST("/DeleteRRelation", Controller.DeleteRRelation)
	re.POST("/EditRRelation", Controller.EditRRelation)
	re.POST("/CountRRelation", Controller.CountRRelation)
}
