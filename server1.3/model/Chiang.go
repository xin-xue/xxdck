package model

type Chiang struct {
	Cno        string `json:"cno"`
	Sno        string `json:"sno"`
	Cname      string `json:"cname"`
	Cbirth     string `json:"cbirth"`
	Cdeath     string `json:"cdeath"`
	Cbiography string `json:"cbiography"`
	Chonor     string `json:"chonor"`
}
