package model

type RRelation struct {
	Cno1 string `json:"cno1"`
	Cno2 string `json:"cno2"`
	Zno  string `json:"zno"`
}
