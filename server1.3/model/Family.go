package model

type Family struct {
	Sno       string `json:"sno"`
	Sname     string `json:"sname"`
	Sdate     string `json:"sdate"`
	Spassword string `json:"spassword"`
}
