package model

type Event struct {
	Cno        string `json:"cno"`
	Eno        string `json:"eno"`
	Edate      string `json:"edate"`
	Edescrible string `json:"edescrible"`
	Eattach    string `json:"eattach"`
}
